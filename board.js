let square = '';

const createAsciiBoard = (col, row) => {

    for (let i = 0; i < row; i++) {

        for(let j = 0; j < col; j++) {

            if (i % 2 > 0) {
                if (j % 2 > 0) {
                    square += ' © ';
                } else {
                    square += ' ■ ';
                }
            } else {
                if (j % 2 === 0) {
                    square += ' © ';
                } else {
                    square += ' ■ ';
                }
            }
        }
        square += '\n';
    }
}

createAsciiBoard(8, 8);
console.log(square);